from app import app #Импорт нашего Flask app
import views

if __name__ == '__main__':
    app.run(host="0.0.0.0", threaded=True)