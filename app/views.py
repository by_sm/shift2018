# This Python file uses the following encoding: utf-8
from flask import render_template, request, redirect, url_for
from app import app, mysql
import time
import hashlib

@app.route('/', methods=['POST', 'GET'])
def homepage():
	"""Стартовая страница. Сохраняет форму с полями note_id и text 
	в базу данных""" 
	if request.method == 'POST':
		text = request.form['text'][:200]
		note_id = hashlib.md5(text.encode()).hexdigest()
		
		try:
			conn1 = mysql.connect()
			with conn1.cursor() as cur:
				sql = "INSERT INTO notes(note_id, text) VALUES ('{}', '{}');".format(note_id, text)
				cur.execute(sql)
			conn1.commit()
			conn1.close()
		
		except Exception as e:
			conn1.close()
			#!костыль обрабатываем повторное добавление в БД
			if "Duplicate entry" in str(e):
				return redirect(url_for('show_note', note=note_id))

			message = '<h1>Hacking attempt!</h1></br>'
			message += '<h2>Ты копаешь не там</h2></br>'
			message += "SQL error {}".format(e)
			drink(request.remote_addr)
			return message
		
		return redirect(url_for('show_note', note=note_id))
	return render_template("home.html")

@app.route('/<note>')
def show_note(note):
	"""Отображаем нашу заметку"""
	is_drink = drink_test(request,request.remote_addr, note)
	
	if is_drink:
		return is_drink

	notes = None

	try:
		conn2 = mysql.connect()
		with conn2.cursor() as cur:
			#SQLi = /something' OR True OR '
			#= /something' OR 1=1--'
			sql = "SELECT * FROM notes WHERE note_id = '{}';".format(note)
			cur.execute(sql)
			notes = cur.fetchall()
		conn2.close()
	except Exception as e:
		conn2.close()
		drink(request.remote_addr, level=0)
		return "SQL error {}".format(e)
	return render_template("note.html", note=note, notes=notes)

def drink_test(request,ip, query):
	if "OR 1=1" in query:
		drink(ip, level=0)
		return '1=1 - не путь воина'

	for command in ['drop', 'benchmark', 'sleep', 'md5']:
		if command in query.lower():
			drink(ip, level=1)
			return 'Вы используете запрещенные методы. Не надо так! <br> Пьешь 2 раза'

	return False

def drink(ip, level=0):
	with open('pastenote.log', 'a') as log:
		t = time.strftime("%H:%M:%S", time.gmtime())
		log.write("TIME {} IP {} LEVEL {}\n".format(t, ip, level))