from flask import Flask
from flaskext.mysql import MySQL
from config import Configuration #Импортируем наш конфигурационный файл

app = Flask(__name__)
app.config.from_object(Configuration) # Используем значения из нашего конфиг файла
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'pass'
app.config['MYSQL_DATABASE_DB'] = 'db'
app.config['MYSQL_DATABASE_HOST'] = 'db'
mysql.init_app(app)