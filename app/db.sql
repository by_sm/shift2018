CREATE TABLE `db`.`notes` (
  `note_id` CHAR(32) NOT NULL UNIQUE,
  `text` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`note_id`));

CREATE DATABASE db;

CREATE TABLE `notes` (
    `note_id` CHAR(32) COLLATE utf8_bin NOT NULL UNIQUE,
    `note` VARCHAR(200) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`note_id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;