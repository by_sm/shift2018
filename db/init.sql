CREATE DATABASE db;
USE db;
CREATE TABLE `notes` (
    `note_id` CHAR(32) COLLATE utf8_bin NOT NULL UNIQUE,
    `text` VARCHAR(200) COLLATE utf8_bin NOT NULL,
    PRIMARY KEY (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
INSERT INTO notes(note_id, text) VALUES("f3g", "flag");